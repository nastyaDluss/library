package by.epam.library.util;

import by.epam.library.model.entity.TypePlace;

import java.util.Calendar;
import java.util.Date;

public class CalenderCalculator {

    private static final int COUNT_DAY_FOR_HOME = 1;
    private static final int COUNT_HOUR_FOR_HALL = 3;
    private static final int COUNT_DAY_FOR_PUT_ASIDE = 1;

    /**
     * Inspire of type place count plan returned date
     * @param type
     * @return
     */
    public static Date calculatePlannedDate(TypePlace type) {
        Date now = new Date( );
        Calendar calendar = Calendar.getInstance( );
        calendar.setTime(now);
        switch(type){
            case HOME:
                calendar.add(Calendar.MONTH, COUNT_DAY_FOR_HOME);
                break;
            case HALL:
                calendar.add(Calendar.HOUR_OF_DAY, COUNT_HOUR_FOR_HALL);
                break;
            case PUT_ASIDE_BOOK:
                calendar.add(Calendar.DAY_OF_MONTH, COUNT_DAY_FOR_PUT_ASIDE);
                break;
            default:
        }
        return calendar.getTime( );
    }

    /**
     * Get current date
     * @return
     */
    public static Date calculateCurrentDate() {
        return new Date();
    }
}
