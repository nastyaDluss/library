package by.epam.library.util;

public class PathManager {
    /**
     * Get path for redirect
     * @param path
     * @return
     */
    public static String getPath(String path) {
        return path.replaceAll("%26", "&");
    }
}
