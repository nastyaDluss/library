package by.epam.library.util.validation;

public class AuthorValidator {

    private static final int MAX_LENGTH_LAST_NAME_AND_FIRST_NAME = 45;

    /**
     * Check parameters of author
     * @param lastName
     * @param firstName
     * @return
     */
    public static boolean validate(String lastName, String firstName) {
        return (!lastName.isEmpty( ) && !firstName.isEmpty( ) &&
                lastName.length( ) <= MAX_LENGTH_LAST_NAME_AND_FIRST_NAME &&
                firstName.length( ) <= MAX_LENGTH_LAST_NAME_AND_FIRST_NAME);
    }

}
