package by.epam.library.util.validation;

public class PersonValidator {

    private static final int MIN_LENGTH_LOGIN_AND_PASSWORD = 6;
    private static final int MAX_LENGTH_LOGIN_AND_PASSWORD = 10;
    private static final int MAX_LENGTH_LAST_AND_FIRST_NAME = 20;

    /**
     * Check parameters of person
     * @param login
     * @param password
     * @param lastName
     * @param firstName
     * @return
     */
    public static boolean validate(String login, String password, String lastName, String firstName) {
        return !login.isEmpty( ) && !password.isEmpty( ) && !lastName.isEmpty( ) && !firstName.isEmpty( ) &&
                login.length( ) >= MIN_LENGTH_LOGIN_AND_PASSWORD && login.length( ) <= MAX_LENGTH_LOGIN_AND_PASSWORD &&
                password.length( ) >= MIN_LENGTH_LOGIN_AND_PASSWORD && password.length( ) <= MAX_LENGTH_LOGIN_AND_PASSWORD &&
                lastName.length( ) <= MAX_LENGTH_LAST_AND_FIRST_NAME && firstName.length( ) <= MAX_LENGTH_LAST_AND_FIRST_NAME;
    }

    /**
     * Check last name and first name of person
     * @param lastName
     * @param firstName
     * @return
     */
    public static boolean validate(String lastName, String firstName) {
        return !lastName.isEmpty( ) && !firstName.isEmpty( ) &&
                lastName.length( ) <= MAX_LENGTH_LAST_AND_FIRST_NAME && firstName.length( ) <= MAX_LENGTH_LAST_AND_FIRST_NAME;
    }
}
