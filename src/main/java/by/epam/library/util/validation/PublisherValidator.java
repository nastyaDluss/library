package by.epam.library.util.validation;

public class PublisherValidator {

    private static final int MAX_LENGTH_NAME_PUBLISHER = 15;

    /**
     * Check parameters of publisher
     * @param name
     * @return
     */
    public static boolean validate(String name) {
        return !name.isEmpty( ) && name.length( ) <= MAX_LENGTH_NAME_PUBLISHER;
    }
}
