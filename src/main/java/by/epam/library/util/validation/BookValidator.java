package by.epam.library.util.validation;

public class BookValidator {

    private static final int MAX_LENGTH_NAME_BOOK = 30;
    private static final int MIN_VALUE_AMOUNT_BOOK = 0;

    /**
     * Check parameters of book
     * @param nameBook
     * @param amount
     * @return
     */
    public static boolean validate(String nameBook, int amount) {
        return !nameBook.isEmpty( ) && nameBook.length( ) <= MAX_LENGTH_NAME_BOOK && amount > MIN_VALUE_AMOUNT_BOOK;
    }
}
