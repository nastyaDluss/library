package by.epam.library.services;

import by.epam.library.dao.ConnectionPool;
import by.epam.library.dao.PersonDAO;
import by.epam.library.model.entity.Person;
import by.epam.library.model.exception.DAOException;
import by.epam.library.model.exception.ServiceException;

import java.sql.Connection;
import java.util.List;

public class PersonService {

    /**
     * Find all librarian
     * @return
     * @throws ServiceException
     */
    public List<Person> findAllLibrarian() throws ServiceException {
        try {
            ConnectionPool connectionPool = ConnectionPool.getInstance( );
            Connection connection = connectionPool.getConnection( );
            PersonDAO personDAO = new PersonDAO(connection);
            connectionPool.returnConnection(connection);
            return personDAO.findAllLibrarian( );
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage( ), e);
        }
    }

    /**
     * Find all reader
     * @return
     * @throws ServiceException
     */
    public List<Person> findAllReaders() throws ServiceException {
        try {
            ConnectionPool connectionPool = ConnectionPool.getInstance( );
            Connection connection = connectionPool.getConnection( );
            PersonDAO personDAO = new PersonDAO(connection);
            connectionPool.returnConnection(connection);
            return personDAO.findAllReader( );
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage( ), e);
        }
    }

    /**
     * Find user in database
     * @param login
     * @param password
     * @return
     * @throws ServiceException
     */
    public Person validateUser(String login, String password) throws ServiceException {
        Person person;
        try {
            ConnectionPool connectionPool = ConnectionPool.getInstance( );
            Connection connection = connectionPool.getConnection( );

            PersonDAO personDAO = new PersonDAO(connection);
            person = personDAO.findPersonByLoginAndPassword(login, password);

            connectionPool.returnConnection(connection);

        } catch (DAOException exception) {
            throw new ServiceException(exception);
        }
        return person;
    }

    /**
     * Find user by id
     * @param id
     * @return
     * @throws ServiceException
     */
    public Person findPersonByID(int id) throws ServiceException {
        Person person;
        try {
            ConnectionPool connectionPool = ConnectionPool.getInstance( );
            Connection connection = connectionPool.getConnection( );

            PersonDAO personDAO = new PersonDAO(connection);
            person = (Person) personDAO.findById(id);

            connectionPool.returnConnection(connection);

        } catch (DAOException exception) {
            throw new ServiceException(exception);
        }
        return person;
    }

    /**
     * Save person in database
     * @param person
     * @throws ServiceException
     */
    public void savePerson(Person person) throws ServiceException {
        try {
            ConnectionPool connectionPool = ConnectionPool.getInstance( );
            Connection connection = connectionPool.getConnection( );

            PersonDAO personDAO = new PersonDAO(connection);
            personDAO.save(person);

            connectionPool.returnConnection(connection);

        } catch (DAOException exception) {
            throw new ServiceException(exception);
        }
    }

    /**
     * Inspire of type of user method is called
     * @param typePerson
     * @return
     * @throws ServiceException
     */
    public List<Person> findAllPersonByType(String typePerson) throws ServiceException {
        List catalog = null;
        if (typePerson.equals("Reader")) {
            catalog = findAllReaders( );
        } else if (typePerson.equals("Librarian")) {
            catalog = findAllLibrarian( );
        }
        return catalog;
    }

    /**
     * Find person by last name, first name and type of person
     * @param typePerson
     * @param lastNamePerson
     * @param firstNamePerson
     * @return
     * @throws ServiceException
     */
    public List<Person> findPersonByLastNamePersonAndFirstNamePersonByType(String typePerson, String lastNamePerson,
                                                                           String firstNamePerson)
            throws ServiceException {
        try {
            ConnectionPool connectionPool = ConnectionPool.getInstance( );
            Connection connection = connectionPool.getConnection( );
            PersonDAO personDAO = new PersonDAO(connection);
            connectionPool.returnConnection(connection);
            return personDAO.findPersonByLastNamePersonAndFirstNamePersonByType
                    (typePerson, lastNamePerson, firstNamePerson);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage( ), e);
        }
    }

    /**
     * Find user by last name and type of person
     * @param typePerson
     * @param lastNamePerson
     * @return
     * @throws ServiceException
     */
    public List<Person> findBookByLastNamePersonByType(String typePerson, String lastNamePerson) throws ServiceException {
        try {
            ConnectionPool connectionPool = ConnectionPool.getInstance( );
            Connection connection = connectionPool.getConnection( );
            PersonDAO personDAO = new PersonDAO(connection);
            connectionPool.returnConnection(connection);
            return personDAO.findBookByLastNamePersonByType(typePerson, lastNamePerson);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage( ), e);
        }
    }

    /**
     * Find book by first name and type of person
     * @param typePerson
     * @param firstNamePerson
     * @return
     * @throws ServiceException
     */
    public List<Person> findBookByFirstNamePersonByType(String typePerson, String firstNamePerson) throws ServiceException {
        try {
            ConnectionPool connectionPool = ConnectionPool.getInstance( );
            Connection connection = connectionPool.getConnection( );
            PersonDAO personDAO = new PersonDAO(connection);
            connectionPool.returnConnection(connection);
            return personDAO.findBookByFirstNamePersonByType(typePerson, firstNamePerson);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage( ), e);
        }
    }
}
