package by.epam.library.services;

import by.epam.library.dao.BookDAO;
import by.epam.library.dao.ConnectionPool;
import by.epam.library.dao.OrderDAO;
import by.epam.library.model.entity.Book;
import by.epam.library.model.entity.Order;
import by.epam.library.model.exception.DAOException;
import by.epam.library.model.exception.ServiceException;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class BookService {

    /**
     * Find book by ID
     * @param id
     * @return
     * @throws ServiceException
     */
    public Book findBookByID(int id) throws ServiceException {
        ConnectionPool connectionPool = null;
        Connection connection = null;
        try {
            connectionPool = ConnectionPool.getInstance( );
            connection = connectionPool.getConnection( );
            BookDAO bookDAO = new BookDAO(connection);
            return (Book) bookDAO.findById(id);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage( ), e);
        } finally {
            if (connectionPool != null) {
                connectionPool.returnConnection(connection);
            }
        }
    }

    /**
     * Find all book
     * @return
     * @throws ServiceException
     */
    public List findAllBook() throws ServiceException {
        ConnectionPool connectionPool = null;
        Connection connection = null;
        try {
            connectionPool = ConnectionPool.getInstance( );
            connection = connectionPool.getConnection( );
            BookDAO bookDAO = new BookDAO(connection);
            return bookDAO.findAll();
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage( ), e);
        } finally {
            if (connectionPool != null) {
                connectionPool.returnConnection(connection);
            }
        }
    }

    /**
     * Order book
     * @param id
     * @param order
     * @throws ServiceException
     */
    public void decrementAmountBook(int id, Order order) throws ServiceException {
        ConnectionPool connectionPool = null;
        Connection connection = null;
        try {
            connectionPool = ConnectionPool.getInstance( );
            connection = connectionPool.getConnection( );
            connection.setAutoCommit(false);
            BookDAO bookDAO = new BookDAO(connection);
            Book book = (Book) bookDAO.findById(id);

            int amountBook = book.getAmount( );
            int newAmountBook = amountBook - 1;
            book.setAmount(newAmountBook);

            bookDAO.save(book);

            OrderDAO orderDAO = new OrderDAO(connection);
            orderDAO.save(order);

            connection.commit( );

        } catch (DAOException | SQLException e) {
            throw new ServiceException(e.getMessage( ), e);
        } finally {

                connectionPool.returnConnection(connection);

        }
    }

    /**
     * Take book in the library
     * @param id
     * @param order
     * @throws ServiceException
     */
    public void incrementAmountBook(int id, Order order) throws ServiceException {
        ConnectionPool connectionPool = null;
        Connection connection = null;
        try {
            connectionPool = ConnectionPool.getInstance( );
            connection = connectionPool.getConnection( );
            connection.setAutoCommit(false);
            BookDAO bookDAO = new BookDAO(connection);
            Book book = (Book) bookDAO.findById(id);

            int amountBook = book.getAmount( );
            int newAmountBook = amountBook + 1;
            book.setAmount(newAmountBook);

            bookDAO.save(book);

            OrderDAO orderDAO = new OrderDAO(connection);
            orderDAO.save(order);
            connectionPool.returnConnection(connection);
        } catch (DAOException | SQLException e) {
            throw new ServiceException(e.getMessage( ), e);
        } finally {
            connectionPool.returnConnection(connection);
        }
    }

    /**
     * Search bo by author's last name and nme book
     * @param lastNameAuthor
     * @param nameBook
     * @return
     * @throws ServiceException
     */
    public List findBookByLastNameAuthorAndNameBook(String lastNameAuthor, String nameBook) throws ServiceException {
        ConnectionPool connectionPool = null;
        Connection connection = null;
        try {
            connectionPool = ConnectionPool.getInstance( );
            connection = connectionPool.getConnection( );
            BookDAO bookDAO = new BookDAO(connection);
            return bookDAO.findBookByLastNameAuthorAndNameBook(lastNameAuthor, nameBook);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage( ), e);
        } finally {
            if (connectionPool != null) {
                connectionPool.returnConnection(connection);
            }
        }
    }

    /**
     * Search book by author's last name
     * @param lastNameAuthor
     * @return
     * @throws ServiceException
     */
    public List findBookByLastNameAuthor(String lastNameAuthor) throws ServiceException {
        ConnectionPool connectionPool = null;
        Connection connection = null;
        try {
            connectionPool = ConnectionPool.getInstance( );
            connection = connectionPool.getConnection( );
            BookDAO bookDAO = new BookDAO(connection);
            return bookDAO.findBookByLastNameAuthor(lastNameAuthor);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage( ), e);
        } finally {
            if (connectionPool != null) {
                connectionPool.returnConnection(connection);
            }
        }
    }

    /**
     * Search book by book's name
     * @param nameBook
     * @return
     * @throws ServiceException
     */
    public List findBookByNameBook(String nameBook) throws ServiceException {
        ConnectionPool connectionPool = null;
        Connection connection = null;
        try {
            connectionPool = ConnectionPool.getInstance( );
            connection = connectionPool.getConnection( );
            BookDAO bookDAO = new BookDAO(connection);
            return bookDAO.findBookByNameBook(nameBook);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage( ), e);
        } finally {
            if (connectionPool != null) {
                connectionPool.returnConnection(connection);
            }
        }

    }

    /**
     * Add book in the database
     * @param book
     * @throws ServiceException
     */
    public void addBook(Book book) throws ServiceException {
        ConnectionPool connectionPool = null;
        Connection connection = null;
        try {
            connectionPool = ConnectionPool.getInstance( );
            connection = connectionPool.getConnection( );
            BookDAO authorDAO = new BookDAO(connection);
            authorDAO.save(book);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage( ), e);
        } finally {
            if (connectionPool != null) {
                connectionPool.returnConnection(connection);
            }
        }
    }

    /**
     * Delete book
     * @param book
     * @throws ServiceException
     */
    public void deletedBook(Book book) throws ServiceException {
        ConnectionPool connectionPool = null;
        Connection connection = null;
        try {
            connectionPool = ConnectionPool.getInstance( );
            connection = connectionPool.getConnection( );
            BookDAO bookDAO = new BookDAO(connection);
            bookDAO.save(book);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage( ), e);
        } finally {
            if (connectionPool != null) {
                connectionPool.returnConnection(connection);
            }
        }
    }
}
