package by.epam.library.model.entity;

public enum TypePlace {
    HALL("hall"), HOME("home"), PUT_ASIDE_BOOK("put_aside"), UNKNOWN("");

    private String value;

    TypePlace(String value) {
        this.value = value;
    }

    public static TypePlace getTypePlace(String value){
        TypePlace[] places = TypePlace.values();
        if(value == null){
            return UNKNOWN;
        }
        for (TypePlace place:places) {
            String placeValue = place.value;
            if (value.equals(placeValue)){
                return place;
            }
        }
        return UNKNOWN;
    }
}
