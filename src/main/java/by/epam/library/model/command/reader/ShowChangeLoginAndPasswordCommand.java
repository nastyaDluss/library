package by.epam.library.model.command.reader;

import by.epam.library.model.command.common.ActionCommand;
import by.epam.library.model.command.util.PageFactory;
import by.epam.library.model.exception.CommandException;
import by.epam.library.model.exception.ServiceException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ShowChangeLoginAndPasswordCommand implements ActionCommand {
    private static final String PAGE_JSP = "pageJSP";
    private static final String CHANGE_LOGIN_AND_PASSWORD = "change_login_and_password";

    /**
     * Show page for change login and password
     * @param request
     * @param response
     * @throws CommandException
     * @throws ServiceException
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException,
            ServiceException, ServletException, IOException {
        PageFactory pageFactory = new PageFactory();
        String page = pageFactory.createPage(CHANGE_LOGIN_AND_PASSWORD);
        request.getSession().setAttribute(PAGE_JSP,page);
        request.getRequestDispatcher(page).forward(request, response);
    }
}
