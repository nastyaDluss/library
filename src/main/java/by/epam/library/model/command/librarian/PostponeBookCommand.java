package by.epam.library.model.command.librarian;

import by.epam.library.model.command.common.ActionCommand;
import by.epam.library.model.command.util.PageFactory;
import by.epam.library.model.entity.Order;
import by.epam.library.model.entity.TypePlace;
import by.epam.library.model.exception.CommandException;
import by.epam.library.model.exception.ServiceException;
import by.epam.library.services.OrderService;
import by.epam.library.util.CalenderCalculator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Date;

public class PostponeBookCommand implements ActionCommand {
    private static final String ID = "id";
    private static final String PAGE_JSP = "pageJSP";

    /**
     * Postpone book
     * @param request
     * @param response
     * @throws CommandException
     * @throws ServiceException
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException,
            ServiceException, ServletException, IOException {
        PageFactory pageFactory = new PageFactory( );
        String page = pageFactory.createPage("librarian");

        String stringId = request.getParameter(ID);
        int idOrder = Integer.parseInt(stringId);

        OrderService orderService = new OrderService();
        Order order = orderService.findOrderById(idOrder);
        TypePlace typePlace = TypePlace.PUT_ASIDE_BOOK;

        Date datePlannedHandOutDate = CalenderCalculator.calculatePlannedDate(typePlace);
        Timestamp plannedHandOutDate = new Timestamp(datePlannedHandOutDate.getTime());
        order.setPlannedHandOutDate(plannedHandOutDate);

        orderService.saveOrder(order);

        request.getSession( ).setAttribute(PAGE_JSP, page);
        response.sendRedirect(page);
    }
}
