package by.epam.library.model.command.admin;

import by.epam.library.model.command.common.ActionCommand;
import by.epam.library.model.command.util.PageFactory;
import by.epam.library.model.entity.Author;
import by.epam.library.model.entity.Book;
import by.epam.library.model.entity.Publisher;
import by.epam.library.model.exception.CommandException;
import by.epam.library.model.exception.ServiceException;
import by.epam.library.services.BookService;
import by.epam.library.util.validation.BookValidator;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class EditBookCommand implements ActionCommand {

    private static final String SHOW_ADD_OR_EDIT_BOOK = "admin";
    private static final String PAGE_JSP = "pageJSP";
    private static final String ID_BOOK = "idBook";
    private static final String LIST_AUTHOR = "listAuthor";
    private static final String NAME_BOOK = "nameBook";
    private static final String LIST_PUBLISHERS = "listPublishers";
    private static final String AMOUNT = "amount";
    private static final String IS_DELETED = "isDeleted";
    private static final String WRONG_ACTION = "wrongAction";


    /**
     * Edits book in database
     *
     * @param request
     * @param response
     * @throws CommandException
     * @throws ServiceException
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException, ServiceException, ServletException, IOException {
        PageFactory pageFactory = new PageFactory();
        String page = pageFactory.createPage(SHOW_ADD_OR_EDIT_BOOK);

        Book book = new Book();

        String stringIdBook = request.getParameter(ID_BOOK);
        int idBook = Integer.parseInt(stringIdBook);
        book.setId(idBook);

        String stringIdAuthor = request.getParameter(LIST_AUTHOR);
        int idAuthor = Integer.parseInt(stringIdAuthor);
        Author author = new Author();
        author.setId(idAuthor);
        book.setAuthor(author);

        String name = request.getParameter(NAME_BOOK);
        book.setName(name);

        String stringIdPublisher = request.getParameter(LIST_PUBLISHERS);
        int idPublisher = Integer.parseInt(stringIdPublisher);
        Publisher publisher = new Publisher();
        publisher.setId(idPublisher);
        book.setPublisher(publisher);

        String stringAmount = request.getParameter(AMOUNT);
        int amount = Integer.parseInt(stringAmount);
        book.setAmount(amount);

        String stringIsDeleted = request.getParameter(IS_DELETED);
        Boolean isDeleted = Boolean.valueOf(stringIsDeleted);
        book.setDeleted(isDeleted);

        if (BookValidator.validate(name, amount)) {
            BookService bookService = new BookService( );
            bookService.addBook(book);
        } else {
            request.setAttribute(WRONG_ACTION, "Ошибка ввода");
        }

        request.getSession().setAttribute(PAGE_JSP,page);

        response.sendRedirect(page);

    }
}
