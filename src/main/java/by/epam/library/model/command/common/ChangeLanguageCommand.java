package by.epam.library.model.command.common;

import by.epam.library.model.exception.CommandException;
import by.epam.library.model.exception.ServiceException;
import by.epam.library.util.PathManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.jstl.core.Config;
import java.io.IOException;

public class ChangeLanguageCommand implements ActionCommand {

    private static final String LANGUAGE = "language";

    /**
     * Change language in the system
     *
     * @param request
     * @param response
     * @throws CommandException
     * @throws ServiceException
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException,
            ServiceException, ServletException, IOException {
        String toRedirect = request.getParameter("toredir");
        String path = PathManager.getPath(toRedirect);
        String language = request.getParameter(LANGUAGE);

        Config.set(request.getSession(),Config.FMT_LOCALE, language);
        response.sendRedirect(path);
    }
}
