package by.epam.library.model.command.librarian;

import by.epam.library.model.command.common.ActionCommand;
import by.epam.library.model.command.util.PageFactory;
import by.epam.library.model.exception.CommandException;
import by.epam.library.model.exception.ServiceException;
import by.epam.library.services.OrderService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class CancelOrderCommand implements ActionCommand {

    private static final String ID = "id";
    private static final String PAGE_JSP = "pageJSP";


    /**
     * Cancel order
     *
     * @param request
     * @param response
     * @throws CommandException
     * @throws ServiceException
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException, ServiceException, ServletException, IOException {
        PageFactory pageFactory = new PageFactory( );
        String page = pageFactory.createPage("librarian");

        String stringId = request.getParameter(ID);
        int idOrder = Integer.parseInt(stringId);

        OrderService orderService = new OrderService();
        orderService.deleteOrder(idOrder);

        request.getSession( ).setAttribute(PAGE_JSP, page);
        response.sendRedirect(page);
    }
}
