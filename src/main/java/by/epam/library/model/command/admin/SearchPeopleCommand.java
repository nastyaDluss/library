package by.epam.library.model.command.admin;

import by.epam.library.model.command.common.ActionCommand;
import by.epam.library.model.command.util.PageFactory;
import by.epam.library.model.entity.Person;
import by.epam.library.model.exception.CommandException;
import by.epam.library.model.exception.ServiceException;
import by.epam.library.services.PersonService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class SearchPeopleCommand implements ActionCommand {
    private static final String PAGE_JSP = "pageJSP";
    private static final String CAPTION = "typePage";
    private static final String ENTITIES = "entities";
    private static final String ADMIN_TABLE = "admin_table";
    private static final String LAST_NAME = "last_name";
    private static final String FIRST_NAME = "first_name";
    private static final String TYPE = "type";

    /**
     * Search people
     *
     * @param request
     * @param response
     * @throws CommandException
     * @throws ServiceException
     * @throws ServletException
     * @throws IOException
     */
    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) throws CommandException,
            ServiceException, ServletException, IOException {
        PageFactory pageFactory = new PageFactory( );
        String page = pageFactory.createPage(ADMIN_TABLE);

        String lastNamePerson = request.getParameter(LAST_NAME);
        String firstNamePerson = request.getParameter(FIRST_NAME);
        String typePerson = request.getParameter(TYPE);

        List<Person> catalog = null;
        PersonService personService = new PersonService( );
        if ((lastNamePerson == null && firstNamePerson == null) || (lastNamePerson.isEmpty( ) && firstNamePerson.isEmpty( ))) {
            catalog = personService.findAllPersonByType(typePerson);
        } else if (!lastNamePerson.isEmpty( ) && !firstNamePerson.isEmpty( )) {
            catalog = personService.findPersonByLastNamePersonAndFirstNamePersonByType(typePerson, lastNamePerson, firstNamePerson);
        } else if (!lastNamePerson.isEmpty( )) {
            catalog = personService.findBookByLastNamePersonByType(typePerson, lastNamePerson);
        } else {
            catalog = personService.findBookByFirstNamePersonByType(typePerson, firstNamePerson);
        }

        request.setAttribute(CAPTION, typePerson);
        request.setAttribute(ENTITIES, catalog);
        request.getSession( ).setAttribute(PAGE_JSP, page);
        request.getRequestDispatcher(page).forward(request, response);
    }
}
