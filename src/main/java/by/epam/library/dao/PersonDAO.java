package by.epam.library.dao;

import by.epam.library.model.entity.Person;
import by.epam.library.model.exception.DAOException;
import by.epam.library.util.builder.PersonBuilder;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class PersonDAO extends AbstractDAO {

    private static final String QUERY_FIND_TYPE_PERSON = "SELECT id_person, role, login, password, last_name_person," +
            " first_name_person FROM person WHERE login=? AND password=md5(?) AND is_deleted=0";
    private static final String FIND_ALL_READER_QUERY = "SELECT * FROM person WHERE role='reader' AND is_deleted=0 " +
            "ORDER BY last_name_person";
    private static final String FIND_ALL_LIBRARIAN_QUERY = "SELECT * FROM person WHERE role='librarian' AND " +
            "is_deleted=0 ORDER BY last_name_person";
    private static final String INSERT_QUERY = "INSERT INTO library.person(role, login, password, last_name_person, " +
            "first_name_person, is_deleted) VALUES(?, ?, md5(?), ?, ?, ?)";
    private static final String UPDATE_QUERY = "UPDATE library.person SET role=?, login=?, password=md5(?), " +
            "last_name_person=?, first_name_person=?, is_deleted=? WHERE id_person=?";
    private static final String FIND_PERSON_BY_ID = "SELECT * FROM library.person WHERE id_person=? AND is_deleted=0";
    private static final String FIND_PERSON_BY_TYPE_AND_LAST_NAME = "SELECT * FROM library.person WHERE is_deleted=0" +
            " AND role=? AND last_name_person=?";
    private static final String FIND_PERSON_BY_TYPE_AND_FIRST_NAME = "SELECT * FROM library.person WHERE is_deleted=0" +
            " AND first_name_person=?";
    private static final String FIND_PERSON_BY_TYPE_AND_LAST_NAME_AND_FIRST_NAME = "SELECT * FROM library.person WHERE " +
            "is_deleted=0 AND last_name_person=? AND first_name_person=?";


    public PersonDAO(Connection connection) {
        super(connection);
    }

    /**
     * Return object Person from Result Set
     *
     * @param resultSet
     * @return object
     * @throws DAOException
     */
    @Override
    public Object buildEntity(ResultSet resultSet) throws DAOException {
        try {
            PersonBuilder personBuilder = new PersonBuilder( );
            return personBuilder.buildObject(resultSet);
        } catch (SQLException e) {
            throw new DAOException(e.getMessage( ), e);
        }
    }

    /**
     * Find person by last name and first name
     * @param type
     * @param lastName
     * @param firstName
     * @return list of person
     * @throws DAOException
     */
    public List findPersonByLastNamePersonAndFirstNamePersonByType(String type, String lastName, String firstName)
            throws DAOException {
        return execute(FIND_PERSON_BY_TYPE_AND_LAST_NAME_AND_FIRST_NAME, type, lastName, firstName);
    }

    /**
     * Find persons by type and last name
     * @param type
     * @param lastName
     * @return list of person
     * @throws DAOException
     */
    public List findBookByLastNamePersonByType(String type, String lastName) throws DAOException {
        return execute(FIND_PERSON_BY_TYPE_AND_LAST_NAME, type, lastName);
    }

    /**
     * Find person by first name and type
     * @param type
     * @param firstName
     * @return list of person
     * @throws DAOException
     */
    public List findBookByFirstNamePersonByType(String type, String firstName) throws DAOException {
        return execute(FIND_PERSON_BY_TYPE_AND_FIRST_NAME, type, firstName);
    }

    /**
     * Find all readers
     * @return list of person
     * @throws DAOException
     */
    public List findAllReader() throws DAOException {
        return execute(FIND_ALL_READER_QUERY);
    }

    /**
     * Find all librarians
     * @return list of person
     * @throws DAOException
     */
    public List findAllLibrarian() throws DAOException {
        return execute(FIND_ALL_LIBRARIAN_QUERY);
    }

    /**
     * determines whether to save or update data in the database
     * @param entity
     * @throws DAOException
     */
    @Override
    public void save(Object entity) throws DAOException {
        Person person = (Person) entity;
        Integer personId = person.getId( );
        if (personId == null) {
            change(INSERT_QUERY, person.getRole( ).name(), person.getLogin( ), person.getPassword( ), person.getLastName( ),
                    person.getFirstName( ), person.getDeleted( ));
        } else {
            change(UPDATE_QUERY, person.getRole( ).name(), person.getLogin( ), person.getPassword( ), person.getLastName( ),
                    person.getFirstName( ), person.getDeleted( ), person.getId( ));
        }
    }

    /**
     * Find person by id
     * @param id
     * @return person
     * @throws DAOException
     */
    @Override
    public Object findById(int id) throws DAOException {
        return executeObject(FIND_PERSON_BY_ID, id);
    }

    /**
     * Find all person
     * @return list of person
     * @throws DAOException
     */
    @Override
    public List findAll() throws DAOException {
        return null;
    }

    /**
     * Find peson by login and password
     * @param login
     * @param password
     * @return person
     * @throws DAOException
     */
    public Person findPersonByLoginAndPassword(String login, String password) throws DAOException {
        return (Person) executeObject(QUERY_FIND_TYPE_PERSON, login, password);
    }
}
