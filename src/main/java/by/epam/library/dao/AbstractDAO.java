package by.epam.library.dao;

import by.epam.library.model.exception.DAOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Abstract class for class for work with database
 *
 * @param <T>
 */
public abstract class AbstractDAO<T> implements DAO {
    private static final String COUNT = "COUNT(*)";

    protected Connection connection;

    public AbstractDAO() {
    }

    public AbstractDAO(Connection connection) {
        this.connection = connection;
    }

    public Connection getConnection() {
        return connection;
    }

    public abstract T buildEntity(ResultSet resultSet) throws DAOException;

    /**
     * Return single value from the query
     * @param query
     * @param parameters
     * @return
     * @throws DAOException
     */
    public T executeObject(String query, Object... parameters) throws DAOException {
        List <T> objects = execute(query, parameters);
        if(objects.size() == 0){
            return null;
        }
        return objects.get(0);
    }

    /**
     * Return all value from the query
     * @param query
     * @param parameters
     * @return
     * @throws DAOException
     */
    public List<T> execute(String query, Object... parameters) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(query)){
            List<T> entities = new ArrayList<>();

            if(parameters.length > 0){
                insertData(statement, parameters);
            }

            ResultSet resultSet = statement.executeQuery();

            while(resultSet.next()){
                T order = buildEntity(resultSet);
                entities.add(order);
            }

            return entities;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /**
     * Return amount of value that satisfy the request
     * @param query
     * @param parameters
     * @return
     * @throws DAOException
     */
    public int executeScalar(String query, Object... parameters) throws DAOException {
        try (PreparedStatement statement = connection.prepareStatement(query)){

            if(parameters.length > 0){
                insertData(statement, parameters);
            }

            ResultSet resultSet = statement.executeQuery();

            int amount = 0;
            if(resultSet.next()){
                amount = resultSet.getInt(COUNT);
            }

            return amount;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    /**
     * Update value in DB
     * @param query
     * @param parameters
     * @throws DAOException
     */
    public void change(String query, Object... parameters) throws DAOException {
        try(PreparedStatement statement = connection.prepareStatement(query)) {

            insertData(statement, parameters);

            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage(), e);
        }
    }

    /**
     * Insert value in query
     * @param statement
     * @param parameters
     * @throws DAOException
     */
    private void insertData(PreparedStatement statement, Object... parameters) throws DAOException {
        try {
            int countParameters = parameters.length;
            for (int i = 1; i <= countParameters; i++) {
                statement.setObject(i, parameters[i - 1]);
            }
        } catch (SQLException exception) {
            throw new DAOException(exception.getMessage( ), exception);
        }
    }

}
