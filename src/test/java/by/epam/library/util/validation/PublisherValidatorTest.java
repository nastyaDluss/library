package by.epam.library.util.validation;

import org.junit.Assert;
import org.junit.Test;


public class PublisherValidatorTest {
    @Test
    public void shouldReturnFalseWhenNamePublisherEmpty(){
        Assert.assertFalse(PublisherValidator.validate(""));
    }

    @Test
    public void shouldReturnFalseWhenNamePublisherGreater15Symbols(){
        Assert.assertFalse(PublisherValidator.validate("qwertyuioplkjhgfdsa"));
    }

    @Test
    public void shouldReturnTrueWhenCorrectData(){
        Assert.assertTrue(PublisherValidator.validate("BBC"));
    }
}