package by.epam.library.util.validation;

import org.junit.Assert;
import org.junit.Test;

public class BookValidatorTest {

    private static final String CORRECT_NAME_BOOK = "Cinderella";
    private static final int CORRECT_AMOUNT = 4;

    private static final String EMPTY_STRING = "";

    private static final String INCORRECT_NAME_BOOK = "this book title is longer than 30 characters";
    private static final int INCORRECT_AMOUNT = -3;

    @Test
    public void shouldReturnFalseWhenNameBookEmpty(){
        Assert.assertFalse(BookValidator.validate(EMPTY_STRING, CORRECT_AMOUNT));
    }

    @Test
    public void shouldReturnFalseWhenNameBookGreater30Symbols(){
        Assert.assertFalse(BookValidator.validate(INCORRECT_NAME_BOOK, CORRECT_AMOUNT));
    }

    @Test
    public void shouldReturnFalseWhenAmountLess0(){
        Assert.assertFalse(BookValidator.validate(CORRECT_NAME_BOOK, INCORRECT_AMOUNT));
    }

    @Test
    public void shouldReturnTrueWhenCorrectData(){
        Assert.assertTrue(BookValidator.validate(CORRECT_NAME_BOOK, CORRECT_AMOUNT));
    }
}