package by.epam.library.util.validation;

import org.junit.Assert;
import org.junit.Test;

public class PersonValidatorTest {

    private static final String CORRECT_LOGIN = "readers";
    private static final String CORRECT_PASSWORD = "1234567";
    private static final String CORRECT_LAST_NAME = "Smith";
    private static final String CORRECT_FIRST_NAME = "Alex";

    private static final String EMPTY_FIELD = "";

    private static final String LOGIN_AND_PASSWORD_LESS_MIN_VALUE = "read";
    private static final String LOGIN_AND_PASSWORD_GREATER_MAX_VALUE = "readersreaders";

    private static final String LAST_AND_FIRST_NAME_GREATER_MAX_VALUE = "SmithSmithSmithSmithSmith";

    @Test
    public void shouldReturnFalseWhenLoginEmptyWithFourParameters(){
        Assert.assertFalse(PersonValidator.validate(EMPTY_FIELD, CORRECT_PASSWORD, CORRECT_LAST_NAME, CORRECT_FIRST_NAME));
    }

    @Test
    public void shouldReturnFalseWhenPasswordEmptyWithFourParameters(){
        Assert.assertFalse(PersonValidator.validate(CORRECT_LOGIN, EMPTY_FIELD, CORRECT_LAST_NAME, CORRECT_FIRST_NAME));
    }

    @Test
    public void shouldReturnFalseWhenLastNameEmpty(){
        Assert.assertFalse(PersonValidator.validate(CORRECT_LOGIN, CORRECT_PASSWORD, EMPTY_FIELD, CORRECT_FIRST_NAME));
    }

    @Test
    public void shouldReturnFalseWhenFirstNameEmpty(){
        Assert.assertFalse(PersonValidator.validate(CORRECT_LOGIN, CORRECT_PASSWORD, CORRECT_LAST_NAME, EMPTY_FIELD));
    }

    @Test
    public void shouldReturnFalseWhenLoginEmptyWithTwoParameters(){
        Assert.assertFalse(PersonValidator.validate(EMPTY_FIELD, CORRECT_PASSWORD));
    }

    @Test
    public void shouldReturnFalseWhenPasswordEmptyWithTwoParameters(){
        Assert.assertFalse(PersonValidator.validate(CORRECT_LOGIN, EMPTY_FIELD));
    }

    @Test
    public void shouldReturnFalseWhenWithTwoParameters(){
        Assert.assertFalse(PersonValidator.validate(CORRECT_LOGIN, EMPTY_FIELD));
    }

    @Test
    public void shouldReturnFalseWhenLastNameGreater20Symbols(){
        Assert.assertFalse(PersonValidator.validate(CORRECT_LOGIN, CORRECT_PASSWORD,
                LAST_AND_FIRST_NAME_GREATER_MAX_VALUE, CORRECT_FIRST_NAME));
    }

    @Test
    public void shouldReturnFalseWhenFirstNameGreater20Symbols(){
        Assert.assertFalse(PersonValidator.validate(CORRECT_LOGIN, CORRECT_PASSWORD, CORRECT_LAST_NAME,
                LAST_AND_FIRST_NAME_GREATER_MAX_VALUE));
    }

    @Test
    public void shouldReturnFalseWhenPasswordGreater10SymbolsWithTwoParameters(){
        Assert.assertFalse(PersonValidator.validate(CORRECT_LOGIN, LOGIN_AND_PASSWORD_GREATER_MAX_VALUE));
    }

    @Test
    public void shouldReturnFalseWhenLoginGreater10SymbolsWithTwoParameters(){
        Assert.assertFalse(PersonValidator.validate(LOGIN_AND_PASSWORD_GREATER_MAX_VALUE, CORRECT_PASSWORD));
    }

    @Test
    public void shouldReturnFalseWhenPasswordLess6SymbolsWithTwoParameters(){
        Assert.assertFalse(PersonValidator.validate(CORRECT_LOGIN, LOGIN_AND_PASSWORD_LESS_MIN_VALUE));
    }

    @Test
    public void shouldReturnFalseWhenLoginLess6SymbolsWithTwoParameters(){
        Assert.assertFalse(PersonValidator.validate(LOGIN_AND_PASSWORD_LESS_MIN_VALUE, CORRECT_PASSWORD));
    }

    @Test
    public void shouldReturnFalseWhenPasswordGreater10SymbolsWithFourParameters(){
        Assert.assertFalse(PersonValidator.validate(CORRECT_LOGIN, LOGIN_AND_PASSWORD_GREATER_MAX_VALUE,
                CORRECT_LAST_NAME, CORRECT_FIRST_NAME));
    }

    @Test
    public void shouldReturnFalseWhenLoginGreater10SymbolsWithFourParameters(){
        Assert.assertFalse(PersonValidator.validate(LOGIN_AND_PASSWORD_GREATER_MAX_VALUE, CORRECT_PASSWORD,
                CORRECT_LAST_NAME, CORRECT_FIRST_NAME));
    }

    @Test
    public void shouldReturnFalseWhenPasswordLess6SymbolsWithFourParameters(){
        Assert.assertFalse(PersonValidator.validate(CORRECT_LOGIN, LOGIN_AND_PASSWORD_LESS_MIN_VALUE,
                CORRECT_LAST_NAME, CORRECT_FIRST_NAME));
    }

    @Test
    public void shouldReturnFalseWhenLoginLess6SymbolsWithFourParameters() {
        Assert.assertFalse(PersonValidator.validate(LOGIN_AND_PASSWORD_LESS_MIN_VALUE, CORRECT_PASSWORD,
                CORRECT_LAST_NAME, CORRECT_FIRST_NAME));
    }

    @Test
    public void shouldReturnTrueWhenCorrectDataWithTwoParameters(){
        Assert.assertTrue(PersonValidator.validate(CORRECT_LOGIN, CORRECT_PASSWORD));
    }

    @Test
    public void shouldReturnTrueWhenCorrectDataWithFourParameters(){
        Assert.assertTrue(PersonValidator.validate(CORRECT_LOGIN, CORRECT_PASSWORD,
                CORRECT_LAST_NAME, CORRECT_FIRST_NAME));
    }
}